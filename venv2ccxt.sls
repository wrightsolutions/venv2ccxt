{% set ver_dotted = '1.0.0' %}

{% if salt['file.directory_exists' ]('/opt/local') %}
{% set ccxtsrc_stem = salt['pillar.get']('ccxtsrc_stem','/opt/local/venv2ccxt') %}
{% else %}
{% set ccxtsrc_stem = salt['pillar.get']('ccxtsrc_stem','/tmp') %}
{% endif %}

pkg_venv_plus:
  pkg.installed:
    - pkgs:
      - python-dev
      - python-pip
      - python-virtualenv
#     - python3-virtualenv
#      - virtualenv
#     - python3.4-venv
      - python-matplotlib
#     - zlib1g-dev
#     - libjpeg-dev
#     - python3-pyqt5
#     - pyqt5-dev-tools


ccxtsrc_directory:
  file.directory:
    - name: {{ ccxtsrc_stem }}/
    - user: root
    - group: root
    - mode: 2750
    - clean: False
    - exclude_pat: '*.sh'


ccxtsrc_requirements_txt:
  file.managed:
    - name: {{ ccxtsrc_stem }}/requirements.txt
    - contents: |
        #dnspython==1.15.0
        #ecdsa==0.13
        #pyaes==1.6.1
        #PySocks==1.6.7
        #qrcode==5.3
        #requests==2.18.4
        #six==1.11.0
        #cython
        #configparser
        #pillow
    # - source: salt://fein/settings.py
    - backup: minion
    - require:
      - file: ccxtsrc_directory

# XT as in Exchange trading. Careful not to transpose.
#virtualenv /opt/local/venv2ccxt
#source /opt/local/venv2ccxt/bin/activate
#cd /opt/local/venv2ccxt
#pip install ccxt

